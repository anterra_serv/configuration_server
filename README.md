# 🇺🇸 English (English)
## Repository directory three on web-server
For the update / configuration server, you can use any web-server that supports rewrite.

**Recommended web-server - nginx**

At the time of loading, the application appeal for files of a configuration, the list of sources and logos of branding. At the first start - data come from parameters of build of the application, at the subsequent - or from parameters of build of the application or from the server specified in build.

All manuals on installation, setup of the recommended server, and also technical documentation is available on [Wiki](https://bitbucket.org/anterra_serv/configuration_server/wiki/Home)

- Service Desk - [http://anterra.link/portal](http://anterra.link/portal)

If you do not have a technician staff or you cannot configure the server yourself, you can order a configuration service, the cost is €350

# 🇷🇺 Russian (Русский)
## Репозиторий строения директорий на веб-сервере
Для сервера обновления и конфигурации можно использовать любой веб-сервер, поддерживающий rewrite.

**Рекомендуемый веб-сервер - nginx**

В момент загрузки, приложение обращается за файлами конфигурации, списком источников и логотипов брендирования. При первом запуске - данные берутся из параметров сборки приложения, при последующих - либо из параметров сборки приложения либо с сервера, указанного в сборке.

Все иструкции по установке, настройке рекомендованного сервера, а так же техническая документация по настройке доступна на: [Вики](https://bitbucket.org/anterra_serv/configuration_server/wiki/Home)

- Портал поддержки - [http://anterra.link/portal](http://anterra.link/portal)

Если у вас нет технического специалиста или нет возможности настраивать сервер самим, вы можете заказать услугу по настройке, стоимость 350€